const txtFirstName = document.querySelector("#text-first-name");
const txtLastName = document.querySelector("#text-last-name");
const spanFullName = document.querySelector("#span-full-name");

const updateFullName =()=>{
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`;


};

txtFirstName.addEventListener('keyup', updateFullName);
txtLastName.addEventListener('keyup', updateFullName);