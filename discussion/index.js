//document - it refers to the whole webpage
//querySelector - to select the specific object (HTML elements) from the document(webpage)
const txtFirstName = document.querySelector("#text-first-name");
const spanFullName = document.querySelector("#span-full-name");
const txtLastName = document.querySelector("#text-last-name");
// document.getElementById
// document.getElementByClass
// document.getElementByTagName
// const txtFirstName = document.getElementById("text-first-name");
// const spanFullName = document.getElementById("text-full-name");

//whenever the user interacts with the webpage, this action is consider as an event
//addEventListener - function that teakes two arguments 
//'keyup' = string identify an event
//function that the listener will execute once the specified event is triggered
// txtFirstName && txtLastName.addEventListener('keyup', (event)=>{
// 	spanFullName.innerHTML = txtFirstName.value +" "+ txtLastName.value;
// 		console.log(event.target);
// 		console.log(event.target.value);

// 	})
txtFirstName.addEventListener('keyup', (event)=>{
	//inner HTML - property sets or returns the HTML content
	spanFullName.innerHTML = txtFirstName.value;
	//event.target - contains the element where the event hapen
		console.log(event.target);
		console.log(event.target.value);

	})













